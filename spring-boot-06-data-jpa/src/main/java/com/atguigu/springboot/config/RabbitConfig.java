package com.atguigu.springboot.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

	
	    final static String message1 = "sanguo.message1";
	    final static String message2 = "sanguo.message2";

	    final static String TOPIC_EXCHANGE ="exchange";

	    @Bean
	    public Queue queue1() {
	        return new Queue(message1);
	    }

	    @Bean
	    public Queue queue2() {
	        return new Queue(message2);
	    }

	    @Bean
	    TopicExchange exchange() {
	        return new TopicExchange(TOPIC_EXCHANGE);
	    }

	    @Bean
	    Binding bindingExchangeMessages(Queue queue1, TopicExchange exchange) {
	        System.out.println(queue1.getName());
	        return BindingBuilder.bind(queue1).to(exchange).with("sanguo.#");
	    }

	    @Bean
	    Binding bindingExchangeMessage(Queue queue2, TopicExchange exchange) {
	        System.out.println(queue2.getName());
	        return BindingBuilder.bind(queue2).to(exchange).with("sanguo.message");
	    }
	
	
}
