package com.atguigu.springboot.config;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.atguigu.springboot.entity.UserInfo;

public class UserContext {

    /**
     * 获取当前登录系统用户信息
     */
	public static UserInfo getCurUser(){
		UserInfo userinfo = null;
		try{
			Subject subject = SecurityUtils.getSubject();
			if(subject.isAuthenticated()){
				userinfo =  (UserInfo) subject.getPrincipal();
			}
			return userinfo;
		}catch(Exception e){
			return userinfo;
		}
	}
	
	/**
     * 获取当前登录系统用户Id
     */
	@SuppressWarnings("unlikely-arg-type")
	public static long getCurUserId(){
    	long uid = 0;
        try{
        	
        	UserInfo userInfo =  getCurUser();
        	if(userInfo!=null && !"".equals(userInfo.getUid())){
        		uid = userInfo.getUid();
        	}
            return uid;
        }catch(Exception e){
            return uid;
        }
    }
}
