package com.atguigu.springboot.config.tasks;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.atguigu.springboot.platform.user.service.UserInfoService;
@Component
@EnableScheduling
public class ScheduledTasks {
	    @Autowired
	    UserInfoService userInfoService;
	    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	    
	    @Scheduled(fixedRate = 10000)
	    public void reportCurrentTime() {
	        System.out.println("现在时间：" + dateFormat.format(new Date()));
	        System.out.println("现在时间：" + dateFormat.format(new Date()));
	    }
	    
	    @Scheduled(fixedRate = 10000)
	    public void gelist() {
	    	List<Map<String,Object>> getlist = userInfoService.getlist();
	    	System.out.println(getlist);
	    }
	    
	    
}
