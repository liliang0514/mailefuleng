package com.atguigu.springboot.platform.user.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.atguigu.springboot.config.UserContext;
import com.atguigu.springboot.entity.UserInfo;
import com.atguigu.springboot.platform.user.service.UserInfoService;
  
@Controller
@RequestMapping("/userInfo")
public class UserInfoController {  
    
    @Autowired
    UserInfoService userInfoService;
    
    
    
    /**  
     * 用户查询.  
     * @return  
     */  
    @RequestMapping("/userList")  
    public String userInfo(Model model){  
       List<Map<String,Object>> list=userInfoService.getlist();
       long curUserId = UserContext.getCurUserId();
       System.out.println(curUserId+"================");
       System.out.println(curUserId+"================"+"5555555555");
       System.out.println(curUserId+"================"+"666666666");
       System.out.println(curUserId+"================"+"5555555555");
       model.addAttribute("list",list);
       return "/test/userInfo";  
    }  
     
    /**  
     * 用户添加;  
     * @return  
     */  
    @RequestMapping("/userAdd")  
    @RequiresPermissions("userInfo:add")//权限管理; 
    public String userInfoAdd(UserInfo userInfo){  
    	 boolean result =userInfoService.addUser(userInfo);
       return "/test/userInfoAdd";  
    }  
    /**  
     * 用户删除;  
     * @return  
     */  
    @RequestMapping("/userDel")  
    @RequiresPermissions("userInfo:del")//权限管理;  
    public String userDel(){  
       return "/test/userInfoDel";  
    }  
}