package com.atguigu.springboot.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

public class CommonTool {

	private static SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
	 * 获取UUID
	 */
	public static String getUUID() {
		UUID id = UUID.randomUUID();
		String s = id.toString();
		return s.substring(0, 8) + s.substring(9, 13) + s.substring(14, 18) + s.substring(19, 23) + s.substring(24);
	}

	/**
	 * 判断字符串是否为�?
	 */
	public static boolean strisnull(String str) {
		if (null == str) {
			return true;
		}
		str = str.replaceAll("\t", "");
		str = str.replaceAll("\n", "");
		str = str.replaceAll("\r", "");
		str = str.trim();
		if (str.length() == 0) {
			return true;
		}
		if ("null".equalsIgnoreCase(str)) {
			return true;
		}
		if ("undefined".equalsIgnoreCase(str)) {
			return true;
		}
		return false;
	}

	/**
	 * 获取客户端ip
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
			// 多次反向代理后会有多个ip值，第一个ip才是真实ip
			if (ip.indexOf(",") != -1) {
				ip = ip.split(",")[0];
			}
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("X-Real-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * 获取系统当前时间2018-09-07
	 */
	public static String getCurrentTimeStr_yyyyMMdd() {
		return getTimeStr_yyyyMMdd(new Date());
	}

	/**
	 * 获取时间2018-09-07
	 */
	public static String getTimeStr_yyyyMMdd(Date date) {
		synchronized (yyyyMMddHHmmss) {
			return yyyyMMdd.format(date);
		}
	}

	/**
	 * 获取时间 2018-09-07 14:04:45
	 */
	public static String getTimeStr_yyyyMMddHHmmss(Date date) {
		synchronized (yyyyMMddHHmmss) {
			return yyyyMMddHHmmss.format(date);
		}
	}

	/**
	 * 获取系统当前时间 2018-09-07 14:04:45
	 */
	public static String getCurrentTimeStr_yyyyMMddHHmmss() {
		return getTimeStr_yyyyMMddHHmmss(new Date());
	}


}